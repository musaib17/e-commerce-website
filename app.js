const express = require("express");
const app = express();

const loginRoute = require("./routes/login");
const productsRoute = require("./routes/products");
const productIdRoute = require("./routes/productId");
const checkoutRoute = require("./routes/checkout");
app.use(loginRoute);
app.use(productsRoute);
app.use(productIdRoute);
app.use(checkoutRoute);

app.listen(3000);
module.exports = app;