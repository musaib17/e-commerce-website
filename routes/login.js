const express = require("express");
const router = express.Router();
const { body, validationResult } = require('express-validator');

router.post(
    '/api/login',
    body('email').isEmail().withMessage({ code: 123, message: "Invalid email" }),
    body('password').isLength({ min: 6 }).withMessage({ code: 121, message: "password should be 6 character long" }),
    (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const error = errors.array()
            for (var err of error)
                console.log(err.msg);
            return res.status(400).json({
                success: false,
                error: err.msg
            });
        }
        else {
            const response = {
                email: req.body.email,
                password: req.body.password
            }
            console.log(response);
            res.status(200).send({
                success: true,
                data: response
            })
        }
    },
);

module.exports = router;